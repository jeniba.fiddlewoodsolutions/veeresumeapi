/* eslint-env mocha */
const index = require('../index.js')
const assert = require('assert')
const multer = require("multer");


// var storage = multer.memoryStorage({
//   destination: function (req, file, callback) {
//     callback(null, '');
//   }
// });
// var multipleUpload = multer({ storage: storage }).array('file');
describe('Index', function () {
  this.timeout(35000);
  describe('#handle()', function () {
    this.timeout(30000);
    it('should return Hello World as JSON.', function (done) {
    // this.timeout(15000);
      
      setTimeout(done, 25000);
      index.handler({}, {}, (err, result) => {
        if (err) {
          return done('Err should be null')
        }
        const parsed = JSON.parse(result.body)
        assert.deepEqual(parsed.message, 'Hello Second World')
        return done()
      })
    })
  })
})
