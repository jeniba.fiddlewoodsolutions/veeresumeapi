var mongoose =require( 'mongoose')
var crypto =require( 'crypto')
const PostSchema = new mongoose.Schema({
  text: {
    type: String,
    required: 'Name is required'
  },
  photo:[{
    // data: Buffer,
    ETag: String,
    // description:String,
    Location:String,
    key:String

  }],
  likes: [{type: mongoose.Schema.ObjectId, ref: 'User'}],
  comments: [{
    text: String,
    created: { type: Date, default: Date.now },
    postedBy: { type: mongoose.Schema.ObjectId, ref: 'User'}
  }],
  postedBy: {type: mongoose.Schema.ObjectId, ref: 'User'},
  created: {
    type: Date,
    default: Date.now
  }
})

module.exports =mongoose.model('Post', PostSchema)
